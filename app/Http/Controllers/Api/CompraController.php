<?php

namespace App\Http\Controllers\Api;

use App\Traits\SearchTrait;
use Illuminate\Http\Request;
use App\Traits\RelacionesTrait;
use App\Http\Controllers\Controller;
use App\Models\Compra;
use Illuminate\Support\Facades\Auth;

class CompraController extends Controller
{
    use SearchTrait;
    use RelacionesTrait;

    public function index(Request $request)
    {
        $query = Compra::query();
        $query = $this->search($query, new Compra(), $request->input('search', ''));

        $perPage = $request->input('per_page', $query->count());

        $response =
            $query->OrderBy(
                $request->input('order_by', 'id'),
                $request->input('order_direction', 'DESC')
            )
            ->paginate($perPage);

        return response($response);
    }

    public function show(Compra $compra)
    {
        return response($compra);
    }

    public function store(Request $request)
    {
        $request->request->add([
            'user_id' => Auth::user()->id,
            'fecha_compra' => 'hoy'
        ]);
        $request->validate(Compra::reglas());

        $compra = new Compra();
        $compra->fill($request->input());
        $compra->saveOrFail();

        return response($compra);
    }

    public function update(Compra $compra, Request $request)
    {
        $request->request->add([
            'user_id' => Auth::user()->id,
        ]);
        $request->validate(Compra::reglas($compra));
        $compra->fill($request->input());
        $compra->saveOrFail();

        return response($compra, 201);
    }

    public function destroy(Compra $compra)
    {
        $compra->delete();
        return response([], 204);
    }
}
