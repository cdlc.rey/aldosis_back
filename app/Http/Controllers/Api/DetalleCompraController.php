<?php

namespace App\Http\Controllers\Api;

use App\Traits\SearchTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DetalleCompra;

class DetalleCompraController extends Controller
{
    use SearchTrait;

    public function index(Request $request)
    {
        $query = DetalleCompra::query();
        $query = $this->search($query, new DetalleCompra(), $request->input('search', ''));

        if ($request->has('compra_id') && $request->input('compra_id', 0) > 0) {
            $query->where('compra_id', $request->input('compra_id', 0));
        }
        $perPage = $request->input('per_page', $query->count());

        $response =
            $query->OrderBy(
                $request->input('order_by', 'created_at'),
                $request->input('order_direction', 'DESC')
            )
            ->paginate($perPage);

        return response($response);
    }

    public function show(DetalleCompra $detalleCompra)
    {
        return response($detalleCompra);
    }

    public function store(Request $request)
    {
        $request->validate(DetalleCompra::reglas());

        $detalleCompra = new DetalleCompra();
        $detalleCompra->fill($request->input());
        $detalleCompra->saveOrFail();

        return response($detalleCompra);
    }

    public function update(DetalleCompra $detalleCompra, Request $request)
    {
        $request->validate(DetalleCompra::reglas($detalleCompra));
        $detalleCompra->fill($request->input());
        $detalleCompra->saveOrFail();

        return response($detalleCompra, 201);
    }

    public function destroy(DetalleCompra $detalleCompra)
    {
        $detalleCompra->delete();
        return response([], 204);
    }
}
