<?php

namespace App\Http\Controllers\Api;

use App\Traits\SearchTrait;
use Illuminate\Http\Request;
use App\Models\DetalleEntrega;
use App\Http\Controllers\Controller;

class DetalleEntregaController extends Controller
{
    use SearchTrait;

    public function index(Request $request)
    {
        $query = DetalleEntrega::query();
        $query = $this->search($query, new DetalleEntrega(), $request->input('search', ''));

        if ($request->has('entrega_id') && $request->input('entrega_id', 0) > 0) {
            $query->where('entrega_id', $request->input('entrega_id', 0));
        }
        $perPage = $request->input('per_page', $query->count());

        $response =
            $query->OrderBy(
                $request->input('order_by', 'created_at'),
                $request->input('order_direction', 'DESC')
            )
            ->paginate($perPage);

        return response($response);
    }

    public function show(DetalleEntrega $detalleEntrega)
    {
        return response($detalleEntrega);
    }

    public function store(Request $request)
    {
        $request->validate(DetalleEntrega::reglas());

        $detalleEntrega = new DetalleEntrega();
        $detalleEntrega->fill($request->input());
        $detalleEntrega->saveOrFail();

        return response($detalleEntrega);
    }

    public function update(DetalleEntrega $detalleEntrega, Request $request)
    {
        $request->validate(DetalleEntrega::reglas($detalleEntrega));
        $detalleEntrega->fill($request->input());
        $detalleEntrega->saveOrFail();

        return response($detalleEntrega, 201);
    }

    public function destroy(DetalleEntrega $detalleEntrega)
    {
        $detalleEntrega->delete();
        return response([], 204);
    }
}
