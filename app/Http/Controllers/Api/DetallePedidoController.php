<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\DetallePedido;
use App\Traits\SearchTrait;
use Illuminate\Http\Request;

class DetallePedidoController extends Controller
{
    use SearchTrait;

    public function index(Request $request)
    {
        $query = DetallePedido::query();
        $query = $this->search($query, new DetallePedido(), $request->input('search', ''));

        if ($request->has('pedido_id') && $request->input('pedido_id', 0) > 0) {
            $query->where('pedido_id', $request->input('pedido_id', 0));
        }
        $perPage = $request->input('per_page', $query->count());

        $response =
            $query->OrderBy(
                $request->input('order_by', 'created_at'),
                $request->input('order_direction', 'DESC')
            )
            ->paginate($perPage);

        return response($response);
    }

    public function show(DetallePedido $detallePedido)
    {
        return response($detallePedido);
    }

    public function store(Request $request)
    {
        $request->validate(DetallePedido::reglas());

        $detallePedido = new DetallePedido();
        $detallePedido->fill($request->input());
        $detallePedido->saveOrFail();

        return response($detallePedido);
    }

    public function update(DetallePedido $detallePedido, Request $request)
    {
        $request->validate(DetallePedido::reglas($detallePedido));
        $detallePedido->fill($request->input());
        $detallePedido->saveOrFail();

        return response($detallePedido, 201);
    }

    public function destroy(DetallePedido $detallePedido)
    {
        $detallePedido->delete();
        return response([], 204);
    }
}
