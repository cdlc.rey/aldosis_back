<?php

namespace App\Http\Controllers\Api;

use App\Traits\SearchTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DetalleRecepcion;

class DetalleRecepcionController extends Controller
{
    use SearchTrait;

    public function index(Request $request)
    {
        $query = DetalleRecepcion::query();
        $query = $this->search($query, new DetalleRecepcion(), $request->input('search', ''));

        if ($request->has('recepcion_id') && $request->input('recepcion_id', 0) > 0) {
            $query->where('recepcion_id', $request->input('recepcion_id', 0));
        }
        $perPage = $request->input('per_page', $query->count());

        $response =
            $query->OrderBy(
                $request->input('order_by', 'created_at'),
                $request->input('order_direction', 'DESC')
            )
            ->paginate($perPage);

        return response($response);
    }

    public function show(DetalleRecepcion $detalleRecepcion)
    {
        return response($detalleRecepcion);
    }

    public function store(Request $request)
    {
        $request->validate(DetalleRecepcion::reglas());

        $detalleRecepcion = new DetalleRecepcion();
        $detalleRecepcion->fill($request->input());
        $detalleRecepcion->saveOrFail();

        return response($detalleRecepcion);
    }

    public function update(DetalleRecepcion $detalleRecepcion, Request $request)
    {
        $request->validate(DetalleRecepcion::reglas($detalleRecepcion));
        $detalleRecepcion->fill($request->input());
        $detalleRecepcion->saveOrFail();

        return response($detalleRecepcion, 201);
    }

    public function destroy(DetalleRecepcion $detalleRecepcion)
    {
        $detalleRecepcion->delete();
        return response([], 204);
    }
}
