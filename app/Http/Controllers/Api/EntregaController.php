<?php

namespace App\Http\Controllers\Api;

use App\Models\Compra;
use App\Models\Entrega;
use App\Traits\SearchTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class EntregaController extends Controller
{
    use SearchTrait;
    use RelacionesTrait;

    public function index(Request $request)
    {
        $query = Entrega::query();
        $query = $this->search($query, new Entrega(), $request->input('search', ''));

        $perPage = $request->input('per_page', $query->count());

        $response =
            $query->OrderBy(
                $request->input('order_by', 'id'),
                $request->input('order_direction', 'DESC')
            )
            ->paginate($perPage);

        return response($response);
    }

    public function show(Entrega $entrega)
    {
        return response($entrega);
    }

    public function store(Request $request)
    {
        $request->request->add([
            'user_id' => Auth::user()->id,
            'fecha_entrega' => 'hoy'
        ]);
        $request->validate(Entrega::reglas());

        $entrega = new Entrega();
        $entrega->fill($request->input());
        $entrega->saveOrFail();

        return response($entrega);
    }

    public function update(Entrega $entrega, Request $request)
    {
        $request->request->add([
            'user_id' => Auth::user()->id,
        ]);
        $request->validate(Entrega::reglas());
        $entrega->fill($request->input());
        $entrega->saveOrFail();

        return response($entrega, 201);
    }

    public function destroy(Entrega $entrega)
    {
        $entrega->delete();
        return response([], 204);
    }

    public function entregaCompra(Entrega $entrega, Compra $compra)
    {
        $detalleentrega = $entrega->toArray();
        $compra->detalleCompras()->delete();
        foreach ($detalleentrega['detalle_entrega'] as $key => $value) {
            $compra->detalleCompras()->create([
                'material_id' => $value['material_id'],
                'cantidad' => 0,
                'precio' => 0,
                'precio_total' => 0,
            ]);
        }
        $entrega->compra_id = $compra->getKey();
        $entrega->save();
        $response = [
    		'success'=>	true,
    		'data' => $entrega,
    	];
        return response()->json($response, 200);
    }

    public function cambiarEstado(Entrega $entrega, Request $request)
    {
        $request->request->add([
            'autorizador_id' => Auth::user()->id,
            'fecha_autorizacion' => 'hoy'
        ]);
        $entrega->fill($request->input());
        $entrega->save();
        return response($entrega);
    }
}
