<?php

namespace App\Http\Controllers\Api;

use App\Models\Material;
use App\Models\Movimiento;
use App\Traits\SearchTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Traits\MovimientosTrait;
use Illuminate\Support\Facades\Auth;

class MaterialController extends Controller
{
    use SearchTrait;
    use MovimientosTrait;

    public function index(Request $request)
    {
        $query = Material::query();
        $query = $this->search($query, new Material(), $request->input('search', ''));

        $perPage = $request->input('per_page', $query->count());

        $response =
            $query->OrderBy(
                $request->input('order_by', 'created_at'),
                $request->input('order_direction', 'DESC')
            )
            ->paginate($perPage);

        $response->getCollection()->transform(function ($material){
            $dataInicial = $this->saldoInicial($material->id);
            $material->saldo = $this->saldoPorMaterial($material->id);
            $material->cantidad_inicial = $dataInicial['cantidad_inicial'];
            $material->precio_inicial = $dataInicial['precio_inicial'];
            return $material;
        });

        return response($response);
    }

    public function show(Material $material)
    {
        return response($material);
    }

    public function store(Request $request)
    {
        $request->request->add([
            'user_id' => Auth::user()->id,
        ]);
        $request->validate(Material::reglas());

        $material = new Material();
        $material->fill($request->input());
        $material->saveOrFail();
        $movimiento = new Movimiento();
        $movimiento->create([
            'user_id' => Auth::user()->id,
            'material_id' => $material->id,
            'cantidad_ingreso' => $request->cantidad,
            'cantidad_salida' => 0,
            'precio' => $request->precio ?? 0,
            'cantidad_disponible' => $request->cantidad,
            'tipo' => 'INGRESO'
        ]);
        return response($material);
    }

    public function update(Material $material, Request $request)
    {
        $request->request->add([
            'user_id' => Auth::user()->id,
        ]);
        $request->validate(Material::reglas($material));
        $material->fill($request->input());
        $material->saveOrFail();
        $this->editarSaldoIInicial($material->id, $request->cantidad, $request->precio);

        return response($material, 201);
    }

    public function destroy(Material $material)
    {
        $material->delete();
        return response([], 204);
    }

    public function kardex(Material $material)
    {
        $NewMaterial = $material->toArray();
        $movimientos = Movimiento::where('material_id', $NewMaterial['id'])->get()->toArray();
        $NewMaterial['movimientos'] = $movimientos;
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('Reportes.KardexMaterial', compact('NewMaterial'));
        return $pdf->stream();
    }
}
