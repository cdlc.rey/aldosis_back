<?php

namespace App\Http\Controllers\Api;

use App\Models\Compra;
use App\Models\Pedido;
use App\Traits\SearchTrait;
use Illuminate\Http\Request;
use App\Traits\RelacionesTrait;
use App\Traits\MovimientosTrait;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PedidoController extends Controller
{
    use SearchTrait;
    use RelacionesTrait;
    use MovimientosTrait;

    public function index(Request $request)
    {
        $query = Pedido::query();
        $query = $this->search($query, new Pedido(), $request->input('search', ''));

        $perPage = $request->input('per_page', $query->count());

        $response =
            $query->OrderBy(
                $request->input('order_by', 'id'),
                $request->input('order_direction', 'DESC')
            )
            ->paginate($perPage);

        return response($response);
    }

    public function show(Pedido $pedido)
    {
        return response($pedido);
    }

    public function store(Request $request)
    {
        $request->request->add([
            'user_id' => Auth::user()->id,
            'fecha_pedido' => 'hoy'
        ]);
        $request->validate(Pedido::reglas());

        $pedido = new Pedido();
        $pedido->fill($request->input());
        $pedido->saveOrFail();

        return response($pedido);
    }

    public function update(Pedido $pedido, Request $request)
    {
        $request->request->add([
            'user_id' => Auth::user()->id,
        ]);
        $request->validate(Pedido::reglas());
        $pedido->fill($request->input());
        $pedido->saveOrFail();

        return response($pedido, 201);
    }

    public function destroy(Pedido $pedido)
    {
        $pedido->delete();
        return response([], 204);
    }

    public function pedidoCompra(Pedido $pedido, Compra $compra)
    {
        $detallePedido = $pedido->toArray();
        $compra->detalleCompras()->delete();
        foreach ($detallePedido['detalle_pedido'] as $key => $value) {
            $compra->detalleCompras()->create([
                'material_id' => $value['material_id'],
                'cantidad' => $value['cantidad'],
                'precio' => 0,
                'precio_total' => 0,
            ]);
        }
        $pedido->compra_id = $compra->getKey();
        $pedido->save();
        $response = [
    		'success'=>	true,
    		'data' => $pedido,
    	];
        return response()->json($response, 200);
    }

    public function cambiarEstado(Pedido $pedido, Request $request)
    {
        $request->request->add([
            'autorizador_id' => Auth::user()->id,
            'fecha_autorizacion' => 'hoy'
        ]);
        $pedido->fill($request->input());
        $pedido->save();
        return response($pedido);
    }

    public function buscarDetallePedido(Pedido $pedido, Request $request)
    {
        $detalle = $pedido->detallePedido;
        $lista = $this->totalDetallePedido($detalle);
        return $lista;
    }

    public function terminar(Pedido $pedido, Request $request)
    {
        if ($pedido->estado === 'ENTREGADO') {
            return response(['message' => 'El pedido ya fue entregado'], 404);
        }
        $guardarDetalle = $this->createDetalleEntrega($pedido, $request->input('payload'));
        if ($guardarDetalle['success'] === true) {
            $detalle = $guardarDetalle['items'];
            $entrega = $guardarDetalle['entrega'];
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('Reportes.EntregaMaterial', compact('pedido', 'detalle', 'entrega'));
            $pdf->setPaper(array(0,0,612.00,369.00));
            return $pdf->stream();
        } else {
            return response(['message' => $guardarDetalle['message']], 422);
        }
    }
}
