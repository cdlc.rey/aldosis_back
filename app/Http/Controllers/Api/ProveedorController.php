<?php

namespace App\Http\Controllers\Api;

use App\Models\Proveedor;
use App\Traits\SearchTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProveedorController extends Controller
{
    use SearchTrait;

    public function index(Request $request)
    {
        $query = Proveedor::query();
        $query = $this->search($query, new Proveedor(), $request->input('search', ''));

        $perPage = $request->input('per_page', $query->count());

        $response =
            $query->OrderBy(
                $request->input('order_by', 'created_at'),
                $request->input('order_direction', 'DESC')
            )
            ->paginate($perPage);

        return response($response);
    }

    public function show(Proveedor $proveedor)
    {
        return response($proveedor);
    }

    public function store(Request $request)
    {
        $request->request->add([
            'user_id' => Auth::user()->id,
        ]);
        $request->validate(Proveedor::reglas());

        $proveedor = new Proveedor();
        $proveedor->fill($request->input());
        $proveedor->saveOrFail();

        return response($proveedor);
    }

    public function update(Proveedor $proveedor, Request $request)
    {
        $request->request->add([
            'user_id' => Auth::user()->id,
        ]);
        $request->validate(Proveedor::reglas($proveedor));
        $proveedor->fill($request->input());
        $proveedor->saveOrFail();

        return response($proveedor, 201);
    }

    public function destroy(Proveedor $proveedor)
    {
        $proveedor->load('compras');
        if ($proveedor->compras->count() > 0) {
            return response(['message' => 'No se puede Eliminar al proveedor por que tiene compras registradas'], 422);
        }
        $proveedor->delete();
        return response([], 204);
    }
}
