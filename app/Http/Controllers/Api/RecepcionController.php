<?php

namespace App\Http\Controllers\Api;

use PDF;
use App\Models\Compra;
use App\Models\Recepcion;
use App\Traits\SearchTrait;
use Illuminate\Http\Request;
use App\Traits\RelacionesTrait;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Traits\MovimientosTrait;
use Illuminate\Support\Facades\Auth;

class RecepcionController extends Controller
{
    use SearchTrait;
    use RelacionesTrait;
    use MovimientosTrait;

    public function index(Request $request)
    {
        $query = Recepcion::query();
        $query = $this->search($query, new Recepcion(), $request->input('search', ''));

        $perPage = $request->input('per_page', $query->count());

        $response =
            $query->OrderBy(
                $request->input('order_by', 'id'),
                $request->input('order_direction', 'DESC')
            )
            ->paginate($perPage);

        return response($response);
    }

    public function show(Recepcion $recepcion)
    {
        return response($recepcion);
    }

    public function store(Request $request)
    {
        $request->request->add([
            'user_id' => Auth::user()->id,
            'fecha_recepcion' => 'hoy'
        ]);
        $request->validate(Recepcion::reglas());

        $recepcion = new recepcion();
        $recepcion->fill($request->input());
        $recepcion->saveOrFail();

        return response($recepcion);
    }

    public function update(Recepcion $recepcion, Request $request)
    {
        $request->request->add([
            'user_id' => Auth::user()->id,
        ]);
        $request->validate(Recepcion::reglas($recepcion));
        $recepcion->fill($request->input());
        $recepcion->saveOrFail();

        return response($recepcion, 201);
    }

    public function destroy(Recepcion $recepcion)
    {
        $recepcion->delete();
        return response([], 204);
    }

    public function recepcionCompra(Recepcion $recepcion, Compra $compra)
    {
        $this->createDetalleRecepcion($recepcion, $compra);
        $response = [
    		'success'=>	true,
    		'data' => $recepcion,
    	];
        return response()->json($response, 200);
    }

    public function imprimir(Recepcion $recepcion)
    {
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('Reportes.ReciboMaterial', compact('recepcion'));
        $pdf->setPaper(array(0,0,612.00,369.00));
        return $pdf->stream();
    }
}
