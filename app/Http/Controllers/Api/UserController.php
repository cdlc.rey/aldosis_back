<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Traits\SearchTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    use SearchTrait;

    public function index(Request $request)
    {
        $query = User::query();
        $query = $this->search($query, new User(), $request->input('search', ''));

        $filter = $request->input('filter', '');
        if (!empty($filter)) {
            $filter = '%' . $filter . '%';
            $query->where(function ($query) use ($filter) {
                $query->Where('name', 'like', $filter)
                    ->orWhere('email', 'like', $filter)
                    ->orWhere('tipo', 'like', $filter);
            });
        }
        $perPage = $request->input('per_page', $query->count());

        $response =
            $query->OrderBy(
                $request->input('order_by', 'id'),
                $request->input('order_direction', 'DESC')
            )
            ->paginate($perPage);

        return response($response);
    }

    public function show(User $user)
    {
        return response($user);
    }

    public function store(Request $request)
    {
        $request->validate(User::reglas());

        $user = new User();
        $user->fill($request->input());
        $user->saveOrFail();

        return response($user);
    }

    public function update(User $user, Request $request)
    {
        $request->validate(User::reglas($user));
        $user->fill($request->input());
        $user->saveOrFail();

        return response($user, 201);
    }

    public function destroy(User $user)
    {
        $user->delete();
        return response([], 204);
    }
}
