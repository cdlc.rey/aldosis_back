<?php

namespace App\Http\Controllers;

use App\Models\Movimiento;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function ultimosMovimientos()
    {
        $movimientos = Movimiento::select('material_id')->groupBy('material_id')->limit(5);
    }
}
