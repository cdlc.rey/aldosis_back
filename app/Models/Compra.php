<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

class Compra extends Model
{
    use HasFactory;
    // Tabla
    protected $table = 'compras';

    // relaciones
    public const RELACIONES = ['usuario'];
    protected $with = ['usuario', 'detalleCompras', 'proveedor'];

    // Atributos Asignables
    protected $fillable = [
        'user_id',
        'proveedor_id',
        'factura',
        'fecha_compra'
    ];

    public static function reglas($modelo = null)
    {
        $unico = Rule::unique('compras', 'factura')->ignore($modelo);
        return [
            'user_id'   => ['required', 'exists:users,id'],
            'factura'    => ['required', $unico],
            'fecha_compra'  => ['required'],
        ];
    }

    public function setFechaCompraAttribute($value)
    {
        $this->attributes['fecha_compra'] = date('Y-m-d');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function detalleCompras()
    {
        return $this->hasMany(DetalleCompra::class, 'compra_id');
    }

    public function proveedor()
    {
        return $this->belongsTo(Proveedor::class);
    }
}
