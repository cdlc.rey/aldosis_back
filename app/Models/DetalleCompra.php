<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleCompra extends Model
{
    use HasFactory;

    // Tabla
    protected $table = 'detalle_compras';
    // Relaciones
    protected $with = ['material'];
    public const INCLUDE = ['material', 'compra'];

    // Atributos Asignables
    protected $fillable = [
        'compra_id',
        'material_id',
        'cantidad',
        'precio',
        'precio_total',
    ];
    public static function reglas()
    {
        return [
            'compra_id' => ['sometimes', 'exists:compras,id'],
            'material_id'   => ['sometimes', 'exists:materiales,id'],
            'cantidad'  => ['nullable'],
            'precio'    => ['nullable'],
            'precio_total'  => ['nullable'],
        ];
    }

    public function material()
    {
        return $this->belongsTo(Material::class, 'material_id');
    }

    public function compra()
    {
        return $this->belongsTo(Compra::class, 'compra_id');
    }
}
