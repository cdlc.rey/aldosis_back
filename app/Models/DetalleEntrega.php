<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleEntrega extends Model
{
    use HasFactory;

    // Tabla
    protected $table = 'detalle_entregas';
    // Relaciones
    public const INCLUDE = ['material', ];
    protected $with = ['material'];

    // Atributos Asignables
    protected $fillable = [
        'entrega_id',
        'material_id',
        'cantidad'
    ];

    public static function reglas()
    {
        return [
            'entrega_id'   => ['required', 'exists:entregas,id'],
            'material_id'   => ['required', 'exists:materiales,id'],
            'cantidad' => ['required'],
        ];
    }

    public function entrega()
    {
        return $this->belongsTo(Entrega::class);
    }

    public function material()
    {
        return $this->belongsTo(Material::class, 'material_id');
    }

    public function movimiento()
    {
        return $this->hasOne(Movimiento::class);
    }
}
