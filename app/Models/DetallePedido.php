<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetallePedido extends Model
{
    use HasFactory;

    // Tabla
    protected $table = 'detalle_pedidos';
    // Relaciones
    protected $with = ['material'];
    public const INCLUDE = ['material', 'pedido'];

    // Atributos Asignables
    protected $fillable = [
        'pedido_id',
        'material_id',
        'cantidad',
    ];
    public static function reglas()
    {
        return [
            'pedido_id',
            'material_id',
            'cantidad',
        ];
    }

    public function material()
    {
        return $this->belongsTo(Material::class, 'material_id');
    }

    public function pedido()
    {
        return $this->belongsTo(Pedido::class, 'pedido_id');
    }
}
