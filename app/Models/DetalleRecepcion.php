<?php

namespace App\Models;

use App\Models\Material;
use App\Models\Recepcion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DetalleRecepcion extends Model
{
    use HasFactory;

    // Tabla
    protected $table = 'detalle_recepciones';
    // Relaciones
    protected $with = ['material'];
    public const INCLUDE = ['material', 'recepcion'];

    // Atributos Asignables
    protected $fillable = [
        'recepcion_id',
        'material_id',
        'precio',
        'cantidad',
        'precio_total',
    ];
    public static function reglas()
    {
        return [
            'recepcion_id' => ['required', 'exists:recepciones,id'],
            'material_id' => ['required', 'exists:materiales,id'],
            'precio'    => ['required'],
            'cantidad'  => ['required'],
            'precio_total' => ['required'],
        ];
    }

    public function recepcion()
    {
        return $this->belongsTo(Recepcion::class, 'recepcion_id');
    }

    public function material()
    {
        return $this->belongsTo(Material::class, 'material_id');
    }

    public function movimiento()
    {
        return $this->belongsTo(Movimiento::class);
    }
}
