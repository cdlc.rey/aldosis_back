<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entrega extends Model
{
    use HasFactory;

    // Tabla
    protected $table = 'entregas';
    // Relaciones
    public const INCLUDE = ['usuario'];
    protected $with = ['usuario'];

    // Atributos Asignables
    protected $fillable = [
        'user_id',
        'fecha_entrega',
    ];

    public static function reglas()
    {
        return [
            'user_id'   => ['required', 'exists:users,id'],
            'fecha_entrega' => ['required'],
        ];
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'users');
    }

    public function detalleEntrega()
    {
        return $this->hasMany(DetalleEntrega::class);
    }
}
