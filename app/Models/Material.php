<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Material extends Model
{
    use HasFactory;

    // Tabla
    protected $table = 'materiales';
    // Relaciones
    // protected $with = ['movimientos'];
    public const INCLUDE = ['usuarios'];

    // Atributos Asignables
    protected $fillable = [
        'user_id',
        'codigo',
        'descripcion',
        'marca',
        'color',
        'tamanio',
        'unid_manejo',
        'fecha_registro',
    ];

    public static function reglas($modelo = null)
    {
        $unico = Rule::unique('materiales')->ignore($modelo);
        return [
            'user_id'   => ['required', 'exists:users,id'],
            'codigo' => ['required', $unico],
            'descripcion' => ['required'],
            'marca'     => ['required'],
            'color'     => ['required'],
            'tamanio'   => ['required'],
            'unid_manejo' => ['required'],
            'fecha_registro' => ['required'],
        ];
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'users');
    }

    // public function movimientos()
    // {
    //     return $this->hasMany(Movimiento::class, 'material_id');
    // }
}
