<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model
{
    use HasFactory;

    // Tabla
    protected $table = 'movimientos';
    // Relaciones
    public const INCLUDE = ['usuarios'];
    protected $with = ['material'];

    // Atributos Asignables
    protected $fillable = [
        'user_id',
        'material_id',
        'parent_id',
        'detalle_recepcion_id',
        'detalle_entrega_id',
        'cantidad_ingreso',
        'cantidad_salida',
        'cantidad_disponible',
        'precio',
        'tipo',
    ];

    public static function reglas()
    {
        return [
            'user_id'   => ['required'],
            'material_id'   => ['required'],
            'parent_id' => ['required'],
            'detalle_recepcion_id'  => ['required'],
            'detalle_entrega_id'    => ['required'],
            'cantidad_ingreso'  => ['required'],
            'cantidad_salida'   => ['required'],
            'cantidad_disponible'   => ['required'],
            'precio'    => ['required'],
            'tipo'  => ['required'],
        ];
    }

    public function usuario()
    {
        return $this->belongsTo(User::class);
    }

    public function material()
    {
        return $this->belongsTo(Material::class);
    }

    public function parent()
    {
        return $this->belongsTo(Movimiento::class);
    }

    public function detalleRecepcion()
    {
        return $this->belongsTo(DetalleRecepcion::class);
    }

    public function detalleEntrega()
    {
        return $this->belongsTo(DetalleEntrega::class);
    }
}
