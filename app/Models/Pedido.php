<?php

namespace App\Models;

use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pedido extends Model
{
    use HasFactory;

    // Tabla
    protected $table = 'pedidos';

    // relaciones
    public const RELACIONES = ['usuario', 'autorizador'];
    protected $with = ['detallePedido', 'usuario', 'autorizador'];

    // lista de tipos
    protected const TIPOS = 'COMPRA,PEDIDO';
    protected const ESTADOS = 'SOLICITADO,APROBADO,RECHAZADO';

    // Atributos Asignables
    protected $fillable = [
        'user_id',
        'autorizador_id',
        'fecha_pedido',
        'tipo',
        'estado',
        'fecha_autorizacion',
    ];

    public static function reglas()
    {
        return [
            'user_id'   => ['required', 'exists:users,id'],
            'autorizador_id'    => ['nullable', 'exists:users,id'],
            'fecha_pedido'  => [],
            'tipo'  => ['required', 'in:' . self::TIPOS],
            'estado'    => ['in:' . self::ESTADOS],
            'fecha_autorizacion'    => ['nullable'],
        ];
    }

    public function setFechaPedidoAttribute($value)
    {
        $this->attributes['fecha_pedido'] = date('Y-m-d');
    }

    public function setFechaAutorizacionAttribute($value)
    {
        $this->attributes['fecha_autorizacion'] = date('Y-m-d');
    }

    // Funciones para Relaciones
    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function autorizador()
    {
        return $this->belongsTo(User::class, 'autorizador_id', 'id');
    }

    public function detallePedido()
    {
        return $this->hasMany(DetallePedido::class, 'pedido_id');
    }

    public function pedidoPdf()
    {
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML('<h1>Test</h1>');
        return $pdf->stream();
    }
}
