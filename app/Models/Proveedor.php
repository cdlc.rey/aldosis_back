<?php

namespace App\Models;

use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Proveedor extends Model
{
    use HasFactory;

    // Tabla
    protected $table = 'proveedores';

    // Atributos Asignables
    protected $fillable = [
        'nit',
        'nombre',
        'telefono',
        'direccion',
    ];

    public static function reglas($modelo = null)
    {
        $unico = Rule::unique('proveedores')->ignore($modelo);
        return [
            'nit'   => ['required', 'integer', 'max:9999999999', $unico],
            'nombre'    => ['required', 'max:50'],
            'telefono'  => ['required', 'integer', 'max:9999999999'],
            'direccion' => ['required', 'string', 'max:50'],
        ];
    }

    public function compras()
    {
        return $this->hasMany(Compra::class);
    }
}
