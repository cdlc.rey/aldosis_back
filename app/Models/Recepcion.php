<?php

namespace App\Models;

use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Recepcion extends Model
{
    use HasFactory;

    // Tabla
    protected $table = 'recepciones';

    // relaciones
    // public const RELACIONES = ['usuario'];
    protected $with = ['usuario', 'compra', 'recibeData', 'entregaData', 'detalleRecepcion'];

    // Atributos Asignables
    protected $fillable = [
        'user_id',
        'compra_id',
        'fecha_recepcion',
        'recibe',
        'entrega'
    ];

    public static function reglas($modelo = null)
    {
        $unico = Rule::unique('recepciones', 'compra_id')->ignore($modelo);
        return [
            'user_id'   => ['required', 'exists:users,id'],
            'compra_id' => ['nullable', 'exists:compras,id', $unico],
            'fecha_recepcion' => ['required'],
            'recibe'    => ['required', 'exists:users,id'],
            'entrega'   => ['required', 'exists:users,id']
        ];
    }

    public function setFechaRecepcionAttribute($value)
    {
        $this->attributes['fecha_recepcion'] = date('Y-m-d');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function recibeData()
    {
        return $this->belongsTo(User::class, 'recibe', 'id');
    }

    public function entregaData()
    {
        return $this->belongsTo(User::class, 'entrega', 'id');
    }

    public function detalleRecepcion()
    {
        return $this->hasMany(DetalleRecepcion::class, 'recepcion_id', 'id');
    }

    public function compra()
    {
        return $this->belongsTo(Compra::class, 'compra_id');
    }
}
