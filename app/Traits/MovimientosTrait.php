<?php

namespace App\Traits;

use App\Models\Compra;
use App\Models\Pedido;
use App\Models\Entrega;
use App\Models\Recepcion;
use App\Models\Movimiento;
use Illuminate\Support\Facades\Auth;

/**
 *
 */
trait MovimientosTrait
{
    public function createDetalleRecepcion(Recepcion $recepcion, Compra $compra)
    {
        $detalleCompra = $compra->toArray();
        $this->limpiarDetalle($recepcion);
        foreach ($detalleCompra['detalle_compras'] as $key => $value) {
            $detalle = $recepcion->detalleRecepcion()->create([
                'material_id' => $value['material_id'],
                'cantidad' => $value['cantidad'],
                'precio' => $value['precio'],
                'precio_total' => $value['precio_total'],
            ]);
            // dd($detalle->toArray());
            $detalle->movimiento()->create([
                'user_id' => Auth::user()->id,
                'material_id' => $value['material_id'],
                'detalle_recepcion_id' => $detalle['id'],
                'cantidad_ingreso' => $value['cantidad'],
                'cantidad_salida' => 0,
                'precio' => $value['precio'],
                'cantidad_disponible' => $value['cantidad'],
                'tipo' => 'INGRESO'
            ]);
        }

        $recepcion->compra_id = $compra['id'];
        $recepcion->save();
        return $recepcion;
    }

    private function limpiarDetalle(Recepcion $recepcion)
    {
        $recepcion->detalleRecepcion()->delete();
    }

    public function totalDetallePedido($detalles)
    {
        $listaMaterial = [];
        foreach ($detalles->toArray() as $key => $value) {
            // dd($value);
            $material = Movimiento::where('material_id', $value['material_id'])
                ->where('cantidad_disponible', '!=', '0')
                ->where('tipo', 'INGRESO')->sum('cantidad_disponible')
            ;

            $listaMaterial[] = array_merge($value['material'], [
                'cantidad_disponible' => $material,
                'cantidad' => $value['cantidad']
            ]);
        }
        return $listaMaterial;
    }

    public function validarCantidad($items)
    {
        foreach ($items as $key => $item) {
            if ($item['cantidad'] > $item['cantidad_disponible']) {
                return false;
            }
        }
        return true;
    }

    public function createDetalleEntrega(Pedido $pedido, $items)
    {
        if ($this->validarCantidad($items)) {
            $entrega = Entrega::create([
                'user_id' => Auth::user()->id,
                'fecha_entrega' => date('Y-m-d')
            ]);
            $pedido->estado = 'ENTREGADO';
            $pedido->entrega_id = $entrega->getKey();
            $pedido->save();
            $newItems = [];
            foreach ($items as $key => $item) {
                $detalle = $entrega->detalleEntrega()->create([
                    'cantidad' => $item['cantidad'],
                    'material_id' => $item['id']
                ]);
                $movimientos = Movimiento::where('material_id', $item['id'])
                    ->where('cantidad_disponible', '!=', '0')
                    ->where('tipo', 'INGRESO')->get()
                ;
                $itemCantidad = $item['cantidad'];
                foreach($movimientos as $movimiento) {
                    $cantidad = $movimiento['cantidad_disponible'] - $item['cantidad'];
                    if ($cantidad < 0) {
                        $itemCantidad = $itemCantidad - $movimiento['cantidad_disponible'];
                        $movimiento->cantidad_disponible = $itemCantidad;
                        $movimiento->save();
                        $mov = $this->crearMovimientoSalida($detalle['id'], $movimiento, $movimiento['cantidad_disponible']);
                        array_push($newItems, $mov);
                    } else if ($cantidad === 0) {
                        $movimiento->cantidad_disponible = $cantidad;
                        $movimiento->save();
                        $mov = $this->crearMovimientoSalida($detalle['id'], $movimiento, $item['cantidad']);
                        array_push($newItems, $mov);
                        break;
                    } else {
                        $movimiento->cantidad_disponible = $cantidad;
                        $movimiento->save();
                        $mov = $this->crearMovimientoSalida($detalle['id'], $movimiento, $item['cantidad']);
                        array_push($newItems, $mov);
                        break;
                    }
                };
            }
            return [
                'success' => true,
                'items' => $newItems,
                'entrega' => $entrega
            ];
        } else {
            return [
                'success' => false,
                'message' => 'La cantidad debe ser menor o igual a la cantidad disponible'
            ];
        }

    }

    public function crearMovimientoSalida($entregaId, $movimiento, $cantidad)
    {
        $movimiento = Movimiento::create([
            'user_id' => Auth::user()->id,
            'material_id' => $movimiento['material_id'],
            'parent_id' => $movimiento['id'],
            'detalle_entrega_id' => $entregaId,
            'cantidad_ingreso' => 0,
            'cantidad_salida' => $cantidad,
            'precio' => $movimiento['precio'],
            'cantidad_disponible' => 0,
            'tipo' => 'SALIDA'
        ]);
        return $movimiento;
    }

    public function saldoPorMaterial($materialId)
    {
        $ingresos = Movimiento::where('material_id', $materialId)->sum('cantidad_ingreso');
        $salidas = Movimiento::where('material_id', $materialId)->sum('cantidad_salida');
        return $ingresos - $salidas;
    }

    public function saldoInicial($materialId)
    {
        $movimiento = Movimiento::where('material_id', $materialId)
            ->whereNull('detalle_entrega_id')->whereNull('detalle_recepcion_id')->first()
        ;
        $data = [
            'cantidad_inicial' => $movimiento['cantidad_ingreso'],
            'precio_inicial' => $movimiento['precio']
        ];
        return $data;
    }

    public function editarSaldoIInicial($materialId, $cantidad, $precio)
    {
        $movimiento = Movimiento::where('material_id', $materialId)->get();
        if ($movimiento->count() === 1) {
            $movimiento = $movimiento->first();
            $movimiento->cantidad_ingreso = $cantidad;
            $movimiento->precio = $precio;
            $movimiento->save();
        }
    }
}

