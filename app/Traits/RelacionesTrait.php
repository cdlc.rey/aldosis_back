<?php

namespace App\Traits;

trait RelacionesTrait
{
    public function relaciones($modelo, $relaciones)
    {
        $relacionesArray = [];
        foreach (array_filter(explode(',', $relaciones)) as $include) {
            if (in_array($include, $modelo::RELACIONES)) {
                $relacionesArray[] = $include;
            }
        }
        if ($relaciones !== '') {
            $modelo->load($relacionesArray);
        }
        return $modelo;
    }
}
