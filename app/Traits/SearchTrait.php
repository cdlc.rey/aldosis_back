<?php

namespace App\Traits;

use Exception;
use Illuminate\Support\Arr;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;

trait SearchTrait
{
    protected $searchConditions = [];
    protected $searchEncrypted = [];
    protected $model;
    protected $encrypted = false;

    public function search($query = null, $model = null, $search = null, $submodel = '', $encrypted = false)
    {
        // init data
        $this->searchConditions = [];
        $this->searchEncrypted = [];

        if (!$search) {
            return $query;
        }
        if (!$model) {
            throw new Exception('A model is required', 404);
        }
        $this->model = $model;
        $this->encrypted = $encrypted;
        //validar searchable
        $array = explode(';', $search);
        $this->makeConditions($array);

        if ($submodel !== '') {
            $this->searchConditions = collect($this->searchConditions)->map(function ($condition) use ($submodel) {
                $condition[0] = $submodel . '.' . $condition[0];
                return $condition;
            })->all();
        }
        if (!$query) {
            return $this->searchConditions;
        }
        return $query->where($this->searchConditions);
    }

    public function makeConditions($conditions)
    {
        foreach ($conditions as $value) {
            $boolean = 'and';
            $operator = '=';
            $findOperator = explode(' ', $value);
            $fieldToSearch = '';
            $filter = '';

            if (count($findOperator) > 1) {
                if (strpos($findOperator[1], '|') === false) {
                    $operator = $findOperator[1];
                } else {
                    $replace = trim(str_replace('|', '', $findOperator[1]), ' \t\n\r\0\x0B');
                    $operator = ($replace != '') ? $replace : '=';
                    $boolean = 'or';
                }
                $value = $findOperator[0];
            }

            $fieldValue = explode(':', $value);
            if (!$this->validateField($fieldValue[0], $this->model)) {
                throw new Exception('The field ' . $fieldValue[0] . ' insert in the query no found in the field list', 404);
            }
            if (count($fieldValue) > 2) {
                $filter = array_pop($fieldValue);
                foreach ($fieldValue as $valueFiel) {
                    $fieldToSearch .= $valueFiel . '->';
                }
                $fieldToSearch = trim($fieldToSearch, '->');
                if ($this->encrypted) {
                    array_push($this->searchEncrypted, [
                        $fieldToSearch,
                        $operator,
                        $filter,
                        $boolean
                    ]);
                } else {
                    array_push($this->searchConditions, [
                        $fieldToSearch,
                        $operator,
                        $filter,
                        $boolean
                    ]);
                }
            } else {
                $filter = $fieldValue[1];
                $fieldToSearch = $fieldValue[0];
                array_push($this->searchConditions, [
                    $fieldToSearch,
                    $operator,
                    $filter,
                    $boolean
                ]);
            }
        }
    }

    public function searchWithEncryptedJson($query)
    {
        $conditionsJson = $this->searchEncrypted;

        $data = $query->filter(function ($value, $key) use ($conditionsJson) {
            $exist = true;
            foreach ($conditionsJson as $keyCondition => $condition) {
                $fieldNames = $condition[0];
                $fields = explode('->', $fieldNames);
                $jsonField = array_pop($fields);
                if (!$this->findValue($value->toArray(), $jsonField, $condition[2])) {
                    $exist = false;
                }
            }
            return $exist;
        });
        return collect($data);
    }

    public function findValue($array, $key, $val)
    {
        foreach ($array as $item) {
            if (is_array($item) && $this->findValue($item, $key, $val)) {
                return true;
            }

            if (isset($item[$key]) && $item[$key] == $val) {
                return true;
            }
        }

        return false;
    }

    public function findKey($array, $key)
    {
        foreach ($array as $item) {
            if (is_array($item)) {
                if (key_exists($key, $item)) {
                    return $item[$key];
                    break;
                } else {
                    $partial = $this->findKey($item, $key);
                    if ($partial !== false) {
                        return $partial;
                    }
                }
            }
        }

        return false;
    }

    public function pathKeyArray($array, $searchKey = '', $byValue = false)
    {
        $iter = new RecursiveIteratorIterator(new RecursiveArrayIterator($array), RecursiveIteratorIterator::SELF_FIRST);

        foreach ($iter as $key => $value) {
            //if the key matches our search
            $valueFind = $byValue ? $value : $key;
            if ($valueFind === $searchKey) {
                $keys = array($key);
                for ($i = $iter->getDepth() - 1; $i >= 0; $i--) {
                    array_unshift($keys, $iter->getSubIterator($i)->key());
                }
                return array('path' => implode('.', $keys), 'value' => $value);
            }
        }
        //return false if not found
        return false;
    }

    public function findKeyOrValue($array, $key, $byValue = false, $withBro = false)
    {
        $iter = new RecursiveIteratorIterator(new RecursiveArrayIterator($array), RecursiveIteratorIterator::SELF_FIRST);
        foreach ($iter as $key => $value) {
            //if the key matches our search
            $valueFind = $byValue ? $value : $key;
            if ($valueFind === $key) {
                $keys = array($key);
                for ($i = $iter->getDepth() - 1; $i >= 0; $i--) {
                    array_unshift($keys, $iter->getSubIterator($i)->key());
                }
                if ($withBro) {
                    array_pop($keys);
                    $value = Arr::get($array, implode('.', $keys));
                }
                return array('path' => implode('.', $keys), 'value' => $value);
            }
        }
        //return false if not found
        return false;
    }

    public function validateSearchParameter($search)
    {
        //todo
    }

    public function validateField($field, $model)
    {
        return in_array($field, $model->getFillable());
    }
}
