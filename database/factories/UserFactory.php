<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'area' => $this->faker->randomElement([
                'Presidente',
                'Vicepresidente',
                'Vocal I',
                'Vocal II',
                'Comisión de Constitución Desarrollo',
                'Comisión de planificación',
                'Comisión de infraestructura',
                'Comisión de minería',
                'Comisión de autonomías',
                'Comisión de relaciones gubernamentales',
                'Comisión de desarrollo',
                'Comisión de medio ambiente	',
                'Oficial mayor',
                'Jefe de unidad administrativa y financiera',
                'Encargado área de contrataciones',
                'Encargado área contabilidad',
                'Profesional técnico presupuesto',
                'Profesional I-Sistemas informáticos Activos Fijos y almacenes',
                'Profesional I-Comunicadora Social ',
                'Secretaria Ejecutiva II - Comunicaciones',
                'Secretaria Ejecutiva II – Directiva',
                'Ujier Administración',
                'Chofer Profesional I –Directiva',
                'Chofer Profesional I –Comisiones',
            ]),
            'tipo' => $this->faker->randomElement([
                'JEFATURA',
                'SOLICITANTE',
                'COMPRADOR',
                'ALMACEN',
            ]),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
