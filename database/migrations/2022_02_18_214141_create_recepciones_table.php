<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecepcionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recepciones', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('compra_id')->nullable();
            $table->unsignedBigInteger('recibe');
            $table->unsignedBigInteger('entrega');
            $table->date('fecha_recepcion');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('recibe')->references('id')->on('users');
            $table->foreign('entrega')->references('id')->on('users');
            $table->foreign('compra_id')->references('id')->on('compras');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recepciones');
    }
}
