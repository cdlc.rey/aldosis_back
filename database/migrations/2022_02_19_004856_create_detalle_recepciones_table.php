<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleRecepcionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_recepciones', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('recepcion_id');
            $table->unsignedBigInteger('material_id');
            $table->integer('cantidad');
            $table->decimal('precio', 8, 2);
            $table->decimal('precio_total', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_recepciones');
    }
}
