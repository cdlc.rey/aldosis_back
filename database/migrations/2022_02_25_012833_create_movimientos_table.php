<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('material_id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->unsignedBigInteger('detalle_recepcion_id')->nullable();
            $table->unsignedBigInteger('detalle_entrega_id')->nullable();
            $table->integer('cantidad_ingreso');
            $table->integer('cantidad_salida');
            $table->integer('cantidad_disponible');
            $table->decimal('precio');
            $table->enum('tipo', ['INGRESO', 'SALIDA'])->default('INGRESO');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('material_id')->references('id')->on('materiales');
            $table->foreign('parent_id')->references('id')->on('movimientos');
            $table->foreign('detalle_recepcion_id')->references('id')->on('detalle_recepciones')->onDelete('cascade');
            $table->foreign('detalle_entrega_id')->references('id')->on('detalle_entregas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimientos');
    }
}
