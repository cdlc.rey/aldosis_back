<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Material;
use App\Models\Movimiento;
use App\Models\Proveedor;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $userAdmin = User::factory()->create([
            'name' => 'Alicia Agostopa',
            'email' => 'ali@ali.com',
            'tipo' => 'ADMIN',
            'area' => 'Profesional I-Sistemas informáticos Activos Fijos y almacenes',
            'password' => 'password'
        ]);

        // $user = User::factory()->create([
        //     'name' => 'Usuario Jefatura',
        //     'email' => 'jefatura@ali.com',
        //     'tipo' => 'JEFATURA',
        //     'area' => 'Presidente',
        //     'password' => 'password'
        // ]);

        // $user = User::factory()->create([
        //     'name' => 'Usuario Solicitante',
        //     'email' => 'solicitante@ali.com',
        //     'tipo' => 'SOLICITANTE',
        //     'area' => 'Vocal I',
        //     'password' => 'password'
        // ]);

        // $user = User::factory()->create([
        //     'name' => 'Usuario Comprador',
        //     'email' => 'comprador@ali.com',
        //     'tipo' => 'COMPRADOR',
        //     'area' => 'Chofer Profesional I –Comisiones',
        //     'password' => 'password'
        // ]);

        // $user = User::factory()->create([
        //     'name' => 'Usuario Almacen',
        //     'email' => 'almacen@ali.com',
        //     'tipo' => 'ALMACEN',
        //     'area' => 'Chofer Profesional I –Comisiones',
        //     'password' => 'password'
        // ]);

        // for ($i=0; $i < 6; $i++) {
        //     $material = Material::create([
        //         'user_id' => $userAdmin->id,
        //         'codigo' => 'cod-' . $i,
        //         'descripcion' => 'Material' . $i,
        //         'marca' => 'marca' . $i,
        //         'color' => 'color' . $i,
        //         'tamanio' => 'tamaño' . $i,
        //         'unid_manejo' => 'unidad' . $i,
        //         'fecha_registro' => date('Y-m-d'),
        //     ]);

        //     Movimiento::create([
        //         'user_id' => $userAdmin->id,
        //         'material_id' => $material->id,
        //         'cantidad_ingreso' => 10,
        //         'cantidad_salida' => 0,
        //         'cantidad_disponible' => 10,
        //         'precio' => 20,
        //         'tipo' => 'INGRESO',
        //     ]);
        // }

        // for ($e=0; $e < 3; $e++) {
        //     Proveedor::create([
        //         'nit' => '123' . $e,
        //         'nombre' => 'proveedor' . $e,
        //         'telefono' => '123456' . $e,
        //         'direccion' => 'dirección ' . $e,
        //     ]);
        // }
    }
}
