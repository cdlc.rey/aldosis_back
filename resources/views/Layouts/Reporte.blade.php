<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>A.L.D.O.</title>
        <style type="text/css">
        html{
            margin: 0.5em;
        }
        body {
            font-family: sans-serif;
            font-size: 10pt;
        }

        div.absolute {
            position: absolute;
            /* padding: 0.5em; */
            /* text-align: center; */
            vertical-align: middle;
        }
        /* .table td{
            border: 1px solid black;
            border-spacing: 0px;
            border-collapse: separate;
        } */
        body table th b{
            font-weight: 900;
        }
        header {
            margin: 0;
            max-width: 100%;
            padding-left: 2em;
            padding-top: 2em;
            color: white;
            font-weight: bolder;
            /*overflow: auto;*/
            background-color: #0866A3;
            padding-bottom: 1.6em;
        }
        header .titulo{
            font-size: 34px;
        }
        .container table tr {
            background-color: #eee;
        }
        .container table tr:nth-child(2) {
            background-color: #eee;
        }
        .container table tr:nth-child(even) {
            background-color: #eee;
        }

        .container table tr:nth-child(odd) {
            background-color: #fff;
        }
        .container table {
            table-layout: auto; width: 90%;
        }
        hr{
            border: 2px dotted black;
        }
        /* td{
            /* border: black 2px solid;
        } */
        </style>
    </head>
    <body class="" >
        <header>
            <div>
                <div class="titulo"><b>@yield('titulo')</b></div>
                <div><b>@yield('subTitulo')</b></div>
            </div>
            <div>
                @yield('descripcion')
            </div>
        </header>
        <br/>

        @yield('contenido')
    </body>
</html>
