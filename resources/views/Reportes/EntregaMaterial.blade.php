@extends('Layouts.MediaCarta')

@section('codigoRecibo')
{{$pedido->id}}
@endsection

@section('tituloRecibo')
COMPROBANTE DE ENTREGA
@endsection

@section('contenidoRecibo')

<div class="absolute" style="top: 100px; left: 40px; right: 40px; font-size: 8pt">
    <table>
        <tr>
            <th><b>Fecha:</b></th>
            <td>
                {{Carbon\Carbon::now()->format('d-m-Y H:m:s')}}
            </td>
        <tr>

    </table>

    <hr>

    <table>
        <tr>
            <th><b>De:</b></th>
            <td style="width: 200px">
                {{ \Auth::user()->name }}
            </td>
            <th><b>Área: </b></th>
            <td>
                {{ \Auth::user()->area }}
            </td>
        </tr>
        <tr>
            <th><b>A:</b></th>
            <td>
                {{ $pedido->usuario['name'] }}
            </td>
            <th><b>Área: </b></th>
            <td>
                {{ $pedido->usuario['area'] }}
            </td>
        </tr>
    </table>
    <div class="container">
        <table style="padding-left: 5em">
            <tr>
                <th><b>Id</b></th>
                <th><b>Código</b></th>
                <th><b>Material</b></th>
                <th><b>Unid. de Manejo</b></th>
                <th><b>Cantidad</b></th>
                <th><b>Precio</b></th>
                <th><b>Total</b></th>
            </tr>
            @php
                $total = 0;
            @endphp
            @foreach ($detalle as $item)
            @php
                $total = $total + ($item->cantidad_salida * $item->precio)
            @endphp
            <tr>
                <td style="text-align: center">
                    {{ $item->material['id'] }}
                </td>
                <td style="text-align: center">
                    {{ $item->material['codigo'] }}
                </td>
                <td>
                    {{ $item->material['descripcion'] }}
                </td>
                <td style="text-align: center">
                    {{ $item->material['unid_manejo']  }}
                </td>
                <td style="text-align: center">
                    {{ $item->cantidad_salida }}
                </td>
                <td style="text-align: center">
                    {{ $item->precio }}
                </td>
                <td style="text-align: center">
                    {{ $item->precio * $item->cantidad_salida }}
                </td>
            </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align: right"><b>Total:</b></td>
                <td style="text-align: center">
                    {{ $total }}
                </td>
            </tr>
        </table>
    </div>
</div>
@endsection

@section('pieRecibo')
<table >
    <tr>
        <th style="padding-left: 12em;"><b>Recibí Conforme</b></th>
        <th style="padding-left: 16em;"><b>Entregue Conforme</b></th>

    </tr>
</table>
@endsection
