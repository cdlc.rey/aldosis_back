@extends('Layouts.Reporte')

@section('titulo')
Kardex de Material ID: {{ $NewMaterial['id'] }}
@endsection

@section('subTitulo')
    Detalle Movimiento de Materiales
@endsection

@section('contenido')
    <div class="absolute" style="top: 120px; left: 50px; right: 40px; font-size: 10pt">
        <table>
            <tr>
                <td><b>Descripción:</b></td>
                <td style="width: 200px">
                    {{ $NewMaterial['descripcion'] }}
                </td>
                <td><b>Código:</b></td>
                <td>
                    {{ $NewMaterial['codigo'] }}
                </td>
            <tr>
            <tr>
                <td><b>Color:</b></td>
                <td>
                    {{ $NewMaterial['color'] }}
                </td>
                <td><b>Marca:</b></td>
                <td>
                    {{ $NewMaterial['marca'] }}
                </td>
            <tr>
            <tr>
                <td><b>Tamaño:</b></td>
                <td>
                    {{ $NewMaterial['tamanio'] }}
                </td>
                <td><b>U. de Manejo:</b></td>
                <td>
                    {{ $NewMaterial['unid_manejo'] }}
                </td>
            <tr>
        </table>

        <hr>

        <div class="container" style="padding-left: 5em">
            <table border="2" style="border-collapse: collapse;"  rules="groups">
                <tr>
                    <th colspan="3" scope="row"></th>
                    <th colspan="2"><b>Entradas</b></th>
                    <th colspan="2"><b>Salidas</b></th>
                    <th rowspan="2" bgcolor="#4fc3f7"><b>Saldos</b></th>
                </tr>
                <tr>
                    <th><b>Fecha</b></th>
                    <th><b>Tipo</b></th>
                    <th><b>Precio/U</b></th>
                    <th><b>Cantidad</b></th>
                    <th><b>Total</b></th>
                    <th><b>Cantidad</b></th>
                    <th><b>Total</b></th>
                </tr>
                @php
                    $totalIngresos = 0;
                    $totalPrecioIngresos = 0;
                    $totalSalidas = 0;
                    $totalPrecioSalidas = 0;
                    $saldo = 0;
                @endphp
                @foreach ($NewMaterial['movimientos'] as $detalle)
                @php
                    $totalIngresos = $totalIngresos + $detalle['cantidad_ingreso'];
                    $totalPrecioIngresos = $totalPrecioIngresos + ($detalle['precio'] * $detalle['cantidad_ingreso']);
                    $totalSalidas = $totalSalidas + $detalle['cantidad_salida'];
                    $totalPrecioSalidas = $totalPrecioSalidas + ($detalle['precio'] * $detalle['cantidad_salida']);
                    if ($detalle['tipo'] === 'INGRESO') {
                        $saldo = $saldo + $detalle['cantidad_ingreso'];
                    } else {
                        $saldo = $saldo - $detalle['cantidad_salida'];
                    }
                @endphp
                <tr>
                    <td style="text-align: center">
                        {{ date('d-m-Y', strtotime($detalle['created_at'])) }}
                    </td>
                    <td style="text-align: center">
                        {{ $detalle['tipo'] }}
                    </td>
                    <td style="text-align: center">
                        {{ $detalle['precio'] }}
                    </td>
                    <td style="text-align: center">
                        {{ $detalle['cantidad_ingreso'] }}
                    </td>
                    <td style="text-align: center">
                        {{ $detalle['precio'] * $detalle['cantidad_ingreso'] }}
                    </td>
                    <td style="text-align: center">
                        {{ $detalle['cantidad_salida'] }}
                    </td>
                    <td style="text-align: center">
                        {{ $detalle['precio'] * $detalle['cantidad_salida'] }}
                    </td>
                    <td style="text-align: center" bgcolor="#4fc3f7">
                        {{ $saldo }}
                    </td>
                </tr>
                @endforeach
                <tr>
                    <td></td>
                    <td></td>
                    <td style="text-align: right"><b>Total:</b></td>
                    <td style="text-align: center">
                        <b> {{ $totalIngresos }} </b>
                    </td>
                    <td style="text-align: center">
                        <b> {{ $totalPrecioIngresos }} </b>
                    </td>
                    <td style="text-align: center">
                        <b> {{ $totalSalidas }} </b>
                    </td>
                    <td style="text-align: center">
                        <b> {{ $totalPrecioSalidas }} </b>
                    </td>
                    <td style="text-align: center;" bgcolor="#4fc3f7">
                        <b> {{ $saldo }} </b>
                    </td>
                </tr>
            </table>
        </div>
    </div>
@endsection
