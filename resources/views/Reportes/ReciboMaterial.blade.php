@extends('Layouts.MediaCarta')

@section('codigoRecibo')
{{$recepcion->id}}
@endsection

@section('tituloRecibo')
COMPROBANTE DE RECEPCIÓN
@endsection

@section('contenidoRecibo')

<div class="absolute" style="top: 100px; left: 40px; right: 40px; font-size: 8pt">
    <table>
        <tr>
            <th><b>Fecha:</b></th>
            <td>
                {{Carbon\Carbon::now()->format('d-m-Y H:m:s')}}
            </td>
        <tr>

    </table>

    <hr>

    <table>
        <tr>
            <th><b>De: </b></th>
            <td style="width: 200px">
                {{ $recepcion->entregaData['name'] }}
            </td>
            <th><b>Área: </b></th>
            <td>
                {{ $recepcion->entregaData['area'] }}
            </td>
        </tr>
        <tr>
            <th><b>A: </b></th>
            <td>
                {{ $recepcion->recibeData['name'] }}
            </td>
            <th><b>Área: </b></th>
            <td>
                {{ $recepcion->recibeData['area'] }}
            </td>
        </tr>
    </table>
    <div class="container">
        <table style="padding-left: 5em">
            <tr>
                <th><b>Id</b></th>
                <th><b>Código</b></th>
                <th><b>Material</b></th>
                <th><b>Unid. de Manejo</b></th>
                <th><b>Cantidad</b></th>
                <th><b>Precio</b></th>
                <th><b>Total</b></th>
            </tr>
            @php
                $total = 0;
            @endphp
            @foreach ($recepcion->detalleRecepcion as $detalle)
            @php
                $total = $total + ($detalle->cantidad * $detalle->precio)
            @endphp
            <tr>
                <td style="text-align: center">
                    {{ $detalle->material['id'] }}
                </td>
                <td style="text-align: center">
                    {{ $detalle->material['codigo'] }}
                </td>
                <td>
                    {{ $detalle->material['descripcion'] }}
                </td>
                <td style="text-align: center">
                    {{ $detalle->material['unid_manejo'] }}
                </td>
                <td style="text-align: center">
                    {{ $detalle->cantidad }}
                </td>
                <td style="text-align: center">
                    {{ $detalle->precio }}
                </td>
                <td style="text-align: center">
                    {{ $detalle->precio * $detalle->cantidad }}
                </td>
            </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align: right"><b>Total:</b></td>
                <td style="text-align: center">
                    {{ $total }}
                </td>
            </tr>
        </table>
    </div>
</div>
@endsection

@section('pieRecibo')
<table >
    <tr>
        <th style="padding-left: 12em;"><b>Recibí Conforme</b></th>
        <th style="padding-left: 16em;"><b>Entregue Conforme</b></th>

    </tr>
</table>
@endsection
