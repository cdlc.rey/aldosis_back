<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\CompraController;
use App\Http\Controllers\Api\PedidoController;
use App\Http\Controllers\Api\MaterialController;
use App\Http\Controllers\Api\ProveedorController;
use App\Http\Controllers\Api\RecepcionController;
use App\Http\Controllers\Api\DetalleCompraController;
use App\Http\Controllers\Api\DetallePedidoController;
use App\Http\Controllers\Api\DetalleRecepcionController;
use App\Models\Recepcion;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    //return $request->user();
// });

Route::post('login', [AuthController::class, 'login']);
Route::post('logout', [AuthController::class, 'logout']);

Route::group([
    'middleware' => 'auth:api'
  ], function() {
      Route::get('user', [UserController::class, 'index']);

      // Materiales
      Route::get('materiales', [MaterialController::class, 'index']);
      Route::get('materiales/{material}', [MaterialController::class, 'show']);
      Route::post('materiales', [MaterialController::class, 'store']);
      Route::put('materiales/{material}', [MaterialController::class, 'update']);
      Route::delete('materiales/{material}', [MaterialController::class, 'destroy']);
      Route::get('materiales/{material}/kardex', [MaterialController::class, 'kardex']);

      // Proveedores
      Route::get('proveedores', [ProveedorController::class, 'index']);
      Route::get('proveedores/{proveedor}', [ProveedorController::class, 'show']);
      Route::post('proveedores', [ProveedorController::class, 'store']);
      Route::put('proveedores/{proveedor}', [ProveedorController::class, 'update']);
      Route::delete('proveedores/{proveedor}', [ProveedorController::class, 'destroy']);

      // Pedidos
      Route::get('pedidos/pdf', function () {
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('Reportes.ReciboMaterial');
        return $pdf->stream();
      });
      Route::get('pedidos', [PedidoController::class, 'index']);
      Route::get('pedidos/{pedido}', [PedidoController::class, 'show']);
      Route::post('pedidos', [PedidoController::class, 'store']);
      Route::put('pedidos/{pedido}', [PedidoController::class, 'update']);
      Route::delete('pedidos/{pedido}', [PedidoController::class, 'destroy']);
      Route::get('pedidos/{pedido}/compra/{compra}', [PedidoController::class, 'pedidoCompra']);
      Route::post('pedidos/{pedido}/terminar', [PedidoController::class, 'terminar']);

      // Detalle Pedidos
      Route::get('detalle_pedidos', [DetallePedidoController::class, 'index']);
      Route::get('detalle_pedidos/{detalle_pedido}', [DetallePedidoController::class, 'show']);
      Route::post('detalle_pedidos', [DetallePedidoController::class, 'store']);
      Route::put('detalle_pedidos/{detalle_pedido}', [DetallePedidoController::class, 'update']);
      Route::delete('detalle_pedidos/{detalle_pedido}', [DetallePedidoController::class, 'destroy']);
      Route::put('pedidos/{pedido}/cambiar-estado', [PedidoController::class, 'cambiarEstado']);
      Route::get('pedidos/{pedido}/detalle', [PedidoController::class, 'buscarDetallePedido']);

      // Compras
      Route::get('compras', [CompraController::class, 'index']);
      Route::get('compras/{compra}', [CompraController::class, 'show']);
      Route::post('compras', [CompraController::class, 'store']);
      Route::put('compras/{compra}', [CompraController::class, 'update']);
      Route::delete('compras/{compra}', [CompraController::class, 'destroy']);

      // Detalle Compra
      Route::get('detalle_compras', [DetalleCompraController::class, 'index']);
      Route::get('detalle_compras/{detalle_compra}', [DetalleCompraController::class, 'show']);
      Route::post('detalle_compras', [DetalleCompraController::class, 'store']);
      Route::put('detalle_compras/{detalle_compra}', [DetalleCompraController::class, 'update']);
      Route::delete('detalle_compras/{detalle_compra}', [DetalleCompraController::class, 'destroy']);

      // Recepciones
      Route::get('recepciones', [RecepcionController::class, 'index']);
      Route::get('recepciones/{recepcion}', [RecepcionController::class, 'show']);
      Route::post('recepciones', [RecepcionController::class, 'store']);
      Route::put('recepciones/{recepcion}', [RecepcionController::class, 'update']);
      Route::delete('recepciones/{recepcion}', [RecepcionController::class, 'destroy']);
      Route::get('recepciones/{recepcion}/compra/{compra}', [RecepcionController::class, 'recepcionCompra']);
      Route::get('recepciones/{recepcion}/imprimir', [RecepcionController::class, 'imprimir']);

      // Detalle Recepciones
      Route::get('detalle_recepciones', [DetalleRecepcionController::class, 'index']);
      Route::get('detalle_recepciones/{detalle_recepcion}', [DetalleRecepcionController::class, 'show']);
      Route::post('detalle_recepciones', [DetalleRecepcionController::class, 'store']);
      Route::put('detalle_recepciones/{detalle_recepcion}', [DetalleRecepcionController::class, 'update']);
      Route::delete('detalle_recepciones/{detalle_recepcion}', [DetalleRecepcionController::class, 'destroy']);

      // Usuarios
      Route::get('usuarios', [UserController::class, 'index']);
      Route::get('usuarios/{user}', [UserController::class, 'show']);
      Route::post('usuarios', [UserController::class, 'store']);
      Route::put('usuarios/{user}', [UserController::class, 'update']);
      Route::delete('usuarios/{user}', [UserController::class, 'destroy']);
    //   Route::get('recepciones/{recepcion}/imprimir', function (Recepcion $recepcion) {
    //     // dd($recepcion);
    //     $pdf = App::make('dompdf.wrapper');
    //     $pdf->loadView('Reportes.ReciboMaterial', compact($recepcion));
    //     return $pdf->stream();
    //   });
  });
